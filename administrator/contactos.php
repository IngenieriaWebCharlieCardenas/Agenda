<?php 
	include '../administrator/dll/seguridad.php'; 
	$lista[1]="Administrador";
	$lista[2]="Ayudante";
	extract($_GET);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="UTF-8">
	<title>Administracion</title>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="../recursos/css/estilos.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

</head>
<body>
	<header>
		<h1>Ingreso al sistema</h1>
		<nav>
			<a href="">Usuario</a>
			<a href="contactos.php">Contactos</a>
			<a href="../vistas/nuevo.php">Perfil</a>
			<a href="dll/salir.php">Salir</a>
		</nav>
	</header>
	<main>
		<section class="content">
			<section class="login">
				<h2>Bienvenido <?php echo $lista[$_SESSION['tipo_user']]; ?></h2>
			</section>

			<section class="tablas">
				<h2>Gestion de Contactos</h2>
				<aside><a href="../vistas/nuevo.php">Nuevo Contacto</a></aside>
				<?php 
					$sql="select * from contactos";
					$mi_objeto->consulta($sql);
					$mi_objeto->verconsulta_crud();
					if (@$var==2) {
						$sql="delete from contactos where id=$id_s";
						$mi_objeto->consulta($sql);
						echo "<script>location.href='contactos.php'</script>";
					}
				 ?>
			</section>
		</section>
	</main>
<?php 
	include '../vistas/footer.php';
 ?>
