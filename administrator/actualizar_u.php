<?php 
	include '../administrator/dll/seguridad.php'; 
	//extract($_GET);
	$id_s =$_SESSION['id'];
	$sql="select * from usuarios where id = '$id_s'";
	$mi_objeto->consulta($sql);
	$listacontacto = $mi_objeto->consulta_lista();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="UTF-8">
	<title>Administracion</title>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="../recursos/css/estilos.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

</head>
<body>
	<header>
		<h1>Ingreso al sistema</h1>
		<nav>
			<a href="">Usuario</a>
			<a href="contactos.php">Contactos</a>
			<a href="actualizar_u.php">Perfil</a>
			<a href="dll/salir.php">Salir</a>
		</nav>
	</header>
	<main>
		<section class="content">
			<section class="login">
				<h2>Actualizacion de Contactos</h2>
			</section>

			<section class="tablas">
				<?php 
				if ($listacontacto[9]=="") {
					echo "<img src='../recursos/uploads/avatar.jpg'>";
				}else {
					echo "<img src='../recursos/uploads/$listacontacto[9]'>";
				}
				 ?>
				<form class="formcontacto" method="post" action="actualizar_usuario.php"  enctype="multipart/form-data">
					<input type="hidden" name="id_contacto" value="<?php echo $listacontacto[0]; ?>">
					<input type="text" name="nombre" placeholder="nombre..." value="<?php echo $listacontacto[5]; ?>">
					<input type="text" name="apellido" placeholder="apellido..." value="<?php echo $listacontacto[6]; ?>">
					<input type="email" name="correo" placeholder="correo..." value="<?php echo $listacontacto[1]; ?>">
					<input type="number" name="cedula" placeholder="cedula..." value="<?php echo $listacontacto[7]; ?>">
					<input type="text" name="pais" placeholder="pais..." value="<?php echo $listacontacto[8]; ?>">
					<input type="password" name="clave" placeholder="clave..." value="">
					<select name="estado">

						<option value="<php echo $listacontacto[3] ?>"> 
							<?php 
								$listaselect[1]= "Administrador";
								$listaselect[2]="Visitante";
							echo $listaselect[$listacontacto[3]] ?>
						</option>
						<option value="1">Administrador</option>
						<option value="2">Visitante</option>
					</select>
					<input type="file" name="archivo" class="form-control" id="archivo">
					<button>Guardar</button>
				</form>
			</section>
		</section>
	</main>
<?php 
	include '../vistas/footer.php';
 ?>
