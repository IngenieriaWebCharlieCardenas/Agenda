<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Administracion</title>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="../recursos/css/estilosadmin.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
</head>
<body>
	<div class="logo">Adminstración</div>
	<section class="login-block">
	    <h1>Login</h1>
		<form method="post" action="dll/validar.php">    
		    <input type="email" value="" placeholder="correo" name="correo" id="correo" />
		    <input type="password" value="" placeholder="clave" name="clave" id="clave" />
		    <button>Iniciar Sesion</button>
		</form>
	</section>
</body>
</html>
