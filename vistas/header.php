<?php
include("../controladores/conexion_general.php");
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
	<title><?php echo $site_name;?></title>
	<meta charset="<?php echo $caracteres; ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo $site_path;?>recursos/css/estilos.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

</head>
<body>
	<header>
		<h1>Agenda de contactos</h1>
		<nav>
			<a href="../">Home</a>
			<a href="../vistas/buscar.php">Buscar</a>
			<a href="../vistas/acerca.php">Acerca</a>
			<a href="../administrator/index.php">Admin</a>
		</nav>
	</header>