<?php 
	include '../administrator/dll/seguridad.php';
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta charset="UTF-8">
	<title>Administracion</title>
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" href="../recursos/css/estilos.css">
	<script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>

</head>
<body>
	<header>
		<h1>Ingreso al sistema</h1>
		<nav>
			<a href="">Usuario</a>
			<a href="contactos.php">Contactos</a>
			<a href="../vistas/nuevo.php">Perfil</a>
			<a href="dll/salir.php">Salir</a>
		</nav>
	</header>
<main>
	<section class="content">
		<h2>Crear Nuevos Contactos</h2>
		<form class="formcontacto" method="post" action="../controladores/guardar_contacto.php"  enctype="multipart/form-data">
			<input type="text" name="nombre" placeholder="nombre...">
			<input type="text" name="apellido" placeholder="apellido...">
			<input type="email" name="correo" placeholder="correo...">
			<input type="number" name="cedula" placeholder="cedula...">
			<input type="text" name="telefono" placeholder="telefono...">
			<input type="file" name="archivo" class="form-control" id="archivo">
			<select name="estado">
				<option value="1">Publico</option>
				<option value="2">Oculto</option>
			</select>
			<button>Guardar</button>
		</form>
	</section>
	<img class="avatar" src="../recursos/uploads/avatar.jpg">

</main>
<?php 
	include ("../vistas/footer.php");
 ?>
